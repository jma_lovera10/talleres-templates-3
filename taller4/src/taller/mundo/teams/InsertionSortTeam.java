package taller.mundo.teams;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class InsertionSortTeam extends AlgorithmTeam
{
	public InsertionSortTeam()
	{
		super("Insertion sort (-)");
		userDefined = false;
	}

	@Override
	public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
	{
		return insertionSort(list, orden);
	}

	/**
     Ordena un arreglo de enteros, usando Ordenamiento por inserción.
     @param arr Arreglo de enteros.
	 **/
	private Comparable[] insertionSort(Comparable[] arr, TipoOrdenamiento orden)
	{
		// Para hacer en casa
		Comparable temp = null;

		if(orden.equals(TipoOrdenamiento.ASCENDENTE)){
			for (int i = 1; i < arr.length; i++) {
				for (int j = i; j >0; j--) {
					if(arr[j].compareTo(arr[j-1])<0){
						temp = arr[j];
						arr[j] = arr[j-1];
						arr[j-1] = temp;
						continue;
					}
					break;
				}
			}
		}
		else{
			for (int i = 1; i < arr.length; i++) {
				for (int j = i; j >0; j--) {
					if(arr[j].compareTo(arr[j-1])>0){
						temp = arr[j];
						arr[j] = arr[j-1];
						arr[j-1] = temp;
						continue;
					}
					break;
				}
			}
		}

		return arr;
	}

	private boolean getState(Comparable a, Comparable b, TipoOrdenamiento orden)
	{
		int stat = a.compareTo(b);
		boolean val = stat > 0 && orden == TipoOrdenamiento.ASCENDENTE;
		val = val || stat < 0 && orden == TipoOrdenamiento.DESCENDENTE;
		return val;
	}
	
	public static void main(String[] args) {
		Comparable[] a = {9,7,0,9,3,1,4,2,5,6,6,7,8};
		InsertionSortTeam merge = new InsertionSortTeam();
		a = merge.sort(a, TipoOrdenamiento.ASCENDENTE);
		for (Comparable comparable : a) {
			System.out.print(comparable+",");
		}
	}
}
