package taller.mundo.teams;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class MergeSortTeam extends AlgorithmTeam
{
	public MergeSortTeam()
	{
		super("Merge sort (*)");
		userDefined = true;
	}

	@Override
	public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
	{
		return merge_sort(lista, orden);
	}


	private static Comparable[] merge_sort(Comparable[] lista, TipoOrdenamiento orden)
	{
		// Trabajo en Clase
		
		int mid = lista.length/2;
		if(mid==0)return lista;
		
		Comparable[] izquierda = new Comparable[mid];
		Comparable[] derecha = new Comparable[lista.length-mid];
		int j=0;
		
		for (int i = 0; i < lista.length; i++) {
			if(i<mid)izquierda[i]=lista[i];
			else derecha[j++]=lista[i];
		}
		izquierda = merge_sort(izquierda, orden);
		derecha = merge_sort(derecha, orden);
		lista = merge(izquierda, derecha, orden);
		
		return lista;
	}

	private static Comparable[] merge(Comparable[] izquierda, Comparable[] derecha, TipoOrdenamiento orden)
	{
		// Trabajo en Clase 
		Comparable[] aux = new Comparable[izquierda.length+derecha.length];
		int i=0,j=0;

		if(orden.equals(TipoOrdenamiento.ASCENDENTE)){
			for (int k = 0; k < aux.length; k++) {
				if(i>=izquierda.length)aux[k]=derecha[j++];
				else if(j>=derecha.length)aux[k]=izquierda[i++];
				else if(derecha[j].compareTo(izquierda[i])<0)aux[k]=derecha[j++];
				else aux[k]=izquierda[i++];
			}
		}
		else{
			for (int k = 0; k < aux.length; k++) {
				if(i>=izquierda.length)aux[k]=derecha[j++];
				else if(j>=derecha.length)aux[k]=izquierda[i++];
				else if(derecha[j].compareTo(izquierda[i])>0)aux[k]=derecha[j++];
				else aux[k]=izquierda[i++];
			}
		}
		return aux;
	}

	public static void main(String[] args) {
		Comparable[] a = {9,7,0,9,3,1,4,2,5,6,6,7,8};
		MergeSortTeam merge = new MergeSortTeam();
		a = merge.sort(a, TipoOrdenamiento.ASCENDENTE);
		for (Comparable comparable : a) {
			System.out.print(comparable+",");
		}
	}
}
