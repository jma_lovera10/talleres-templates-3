package taller.mundo.teams;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class BubbleSortTeam extends AlgorithmTeam
{
	public BubbleSortTeam()
	{
		super("Bubble sort (-)");
		userDefined = false;
	}

	@Override
	public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
	{
		return bubbleSort(list, orden);
	}

	/**
     Ordena un arreglo de enteros, usando Bubble Sort.
     @param arr Arreglo de enteros.
	 **/
	private Comparable[] bubbleSort(Comparable[] arr, TipoOrdenamiento orden)
	{
		// Para hacer en casa
		boolean cambio = true;
		int j = 1;
		Comparable temp = null;

		if(orden.equals(TipoOrdenamiento.ASCENDENTE)){

			while(cambio){
				cambio = false;
				for (int i = 0; i < arr.length-j; i++) {
					if(arr[i].compareTo(arr[i+1])>0){
						temp = arr[i+1];
						arr[i+1] = arr[i];
						arr[i] = temp;
						cambio = true;
					}
				}
				j++;
			}
		}
		else{
			while(cambio){
				cambio = false;
				for (int i = 0; i < arr.length-j; i++) {
					if(arr[i].compareTo(arr[i+1])<0){
						temp = arr[i+1];
						arr[i+1] = arr[i];
						arr[i] = temp;
						cambio = true;
					}
				}
				j++;
			}
		}

		return arr;
	}
	
	public static void main(String[] args) {
		Comparable[] a = {9,7,0,9,3,1,4,2,5,6,6,7,8};
		BubbleSortTeam merge = new BubbleSortTeam();
		a = merge.sort(a, TipoOrdenamiento.ASCENDENTE);
		for (Comparable comparable : a) {
			System.out.print(comparable+",");
		}
	}
}
