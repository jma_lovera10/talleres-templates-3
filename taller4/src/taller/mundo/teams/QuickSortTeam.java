package taller.mundo.teams;

import java.util.Random;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class QuickSortTeam extends AlgorithmTeam
{

	private static Random random = new Random();

	public QuickSortTeam()
	{
		super("Quicksort (*)");
		userDefined = true;
	}

	@Override
	public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
	{
		quicksort(list, 0, list.length-1, orden);
		return list;
	}

	private static void quicksort(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
	{
		if(fin<=inicio)return;
		int j = particion(lista, inicio, fin, orden);
		quicksort(lista, inicio, j-1, orden);
		quicksort(lista, j+1, fin, orden);
	}

	private static int particion(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
	{
		int i = inicio, j=fin+1;
		exch(lista,randInt(inicio, fin),inicio);
		Comparable pivot = lista[inicio];

		if(orden.equals(TipoOrdenamiento.ASCENDENTE)){

			while(true){
				while(lista[++i].compareTo(pivot)<0)if(i==fin)break;
				while(pivot.compareTo(lista[--j])<0)if(j==inicio)break;
				if(i>=j)break;
				exch(lista, i, j);
			}
			exch(lista, inicio, j);
			return j;
		}
		else{
			while(true){
				while(lista[++i].compareTo(pivot)>0)if(i==fin)break;
				while(pivot.compareTo(lista[--j])>0)if(j==inicio)break;
				if(i>=j)break;
				exch(lista, i, j);
			}
			exch(lista, inicio, j);
			return j;
		}
	}

	private static int eleccionPivote(int inicio, int fin)
	{
		/**
           Este procedimiento realiza la elecci�n de un �ndice que corresponde al pivote res-
           pecto al cual se realizar�  la partici�n de la lista. Se recomienda escoger el ele-
           mento que se encuentra en la mitad, o de forma aleatoria entre los �ndices [inicio, fin).
		 **/

		return randInt(inicio, fin);
	}

	/**
      Retorna un número aleatorio que se encuentra en el intervalo [min, max]; inclusivo.
      @param min, índice inicial del intervalo.
      @param max, índice final del intervalo.
      @return Un número aleatorio en el intervalo [min, max].
	 **/
	public static int randInt(int min, int max) 
	{
		int randomNum = random.nextInt((max - min)) + min;
		return randomNum;
	}
	
	public static void main(String[] args) {
		Comparable[] a = {9,7,0,9,3,1,4,2,5,6,6,7,8};
		QuickSortTeam.quicksort(a, 0, a.length-1, TipoOrdenamiento.ASCENDENTE);
		for (Comparable comparable : a) {
			System.out.print(comparable+",");
		}
		
	}
}
