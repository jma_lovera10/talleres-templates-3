package taller.mundo.teams;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class SelectionSortTeam extends AlgorithmTeam
{
	public SelectionSortTeam()
	{
		super("Selection Sort (-)");
		userDefined = false;
	}

	@Override
	public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
	{
		return selectionSort(list, orden);
	}

	/**
      Ordena un arreglo de enteros, usando Ordenamiento por selección.
      @param arr Arreglo de enteros.
	 **/
	private Comparable[] selectionSort(Comparable[] arr, TipoOrdenamiento orden)
	{
		// Para hacer en casa
		Comparable temp = null;
		int comp = -1;

		if(orden.equals(TipoOrdenamiento.ASCENDENTE)){
			for (int i = 0; i < arr.length-1; i++) {
				comp = i;
				for (int j = i+1; j < arr.length; j++) {
					if(arr[comp].compareTo(arr[j])>0){
						comp = j;
					}
				}
				temp = arr[i];
				arr[i] = arr[comp];
				arr[comp] = temp;
			}
		}
		else{
			for (int i = 0; i < arr.length-1; i++) {
				comp = i;
				for (int j = i+1; j < arr.length; j++) {
					if(arr[comp].compareTo(arr[j])<0){
						comp = j;
					}
				}
				temp = arr[i];
				arr[i] = arr[comp];
				arr[comp] = temp;
			}
		}
		return arr;
	}
	
	public static void main(String[] args) {
		Comparable[] a = {9,7,0,9,3,1,4,2,5,6,6,7,8};
		SelectionSortTeam merge = new SelectionSortTeam();
		a = merge.sort(a, TipoOrdenamiento.ASCENDENTE);
		for (Comparable comparable : a) {
			System.out.print(comparable+",");
		}
	}
}
