package taller.mundo;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

public class CSVOutput {

	public static void imprimirReporte(String output){
		try{
			File arch = new File("data/reporte.csv");
			if(!arch.exists()){
				arch.createNewFile();
			}
			PrintWriter pw = new PrintWriter(new FileWriter(arch));
			pw.println(output);
			pw.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
