package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;

	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;

	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
	}

	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		try{
			System.out.println("Ingrese el número de jugadores");
			int num = sc.nextInt();
			ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
			for (int i = 0; i < num; i++) {
				System.out.println("Ingrese el nombre del jugador"+(i+1));
				String nombre = sc.next();
				String simbolo = verificarSimbolo(jugadores);
				jugadores.add(new Jugador(nombre, simbolo));
			}
			int x = verificarHorizontal();
			int y = verificarVertical();
			int seguidas = definirSeguidas();
			juego = new LineaCuatro(jugadores, x, y, seguidas);
			juego();
		}
		catch (NumberFormatException e)
		{
			System.out.println("Comando inválido");
		}
	}

	/**
	 * Método que define la cantidad de fichas seguidas para ganar
	 * @return Cantidad de fichas seguidar para ganar el juego
	 */
	public int definirSeguidas() {
		
		int resp =0;
		while(true){
			System.out.println("Ingrese el número de fichas seguidas para ganar.Nota: Mayor a 3");
			resp = sc.nextInt();
			if(resp>=3){
				break;
			}
			System.out.println("El numero es inválido.");
		}
		return resp;
	}
	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		boolean termina = false;
		while(!termina){
			String atacante = juego.darAtacante();
			imprimirTablero();
			System.out.println("Es el turno de "+atacante);
			registrarJugada();
			termina = juego.fin();
		}
		imprimirTablero();
		System.out.println(juego.darAtacante()+" ha ganado!");
		if(reiniciar()){
			empezarJuego();
		}
	}
	
	public boolean reiniciar() {
		
		boolean reiniciar=false;
		
		while(true){
			System.out.println("-----------------------");
			System.out.println("Desea volver a jugar? (Y/N)");
			String res = sc.next().toUpperCase();
			if(res.equals("Y")){
				reiniciar = true;
				break;
			}
			else if (res.equals("N")){
				break;
			}
		}
		return reiniciar;
	}
	
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		try{
			ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
			System.out.println("Ingrese su nombre.");
			String nombre = sc.next();
			String simbolo = verificarSimbolo(jugadores);
			jugadores.add(new Jugador(nombre, simbolo));
			jugadores.add(new Jugador("CPU", escogerSimboloMaquina(simbolo)));
			int x = verificarHorizontal();
			int y = verificarVertical();
			int seguidas = definirSeguidas();
			juego = new LineaCuatro(jugadores, x, y, seguidas);
			juegoMaquina();
		}
		catch (NumberFormatException e)
		{
			System.out.println("Comando inválido");
		}
	}

	/**
	 * Método que escoge el simbolo de la máquina
	 * @return Simbolo elegido para la máquina
	 */
	public String escogerSimboloMaquina(String pSimbolo) {

		String resp = null;

		while(resp==null){
			double val = Math.random()*1000;
			int a = (int) val;
			if(!(a<48||(a>57&&a<65)||(a>90&&a<97)||a>122)){
				char m = (char)a;
				resp = ""+m;
			}
		}

		return resp;
	}

	/**
	 * Método que verifica el simbolo ingresado por el usuario
	 * @return Simbolo elegido
	 */
	public String verificarSimbolo(ArrayList<Jugador> jugadores) {

		String sim = "";

		while(true){
			System.out.println("Ingrese el símbolo que quiere utilizar. Nota: solo un caracter alfa-numérico");
			sim = sc.next();
			if(sim.length()!=1){
				System.out.println("Recuerde solo utilizar un caracter");
				continue;
			}
			char a = sim.charAt(0);
			if(a<48||(a>57&&a<65)||(a>90&&a<97)||a>122){
				System.out.println("Recuerde solo utilizar caracteres alfa-numéricos");
				continue;
			}
			boolean igual = false;
			for (Jugador jugador : jugadores) {
				String symbol = jugador.darSimbolo();
				if(symbol.equals(sim)){
					igual = true;
					System.out.println("Este símbolo ya está en uso.");
					break;
				}
			}
			if(igual){continue;}
			break;
		}
		return sim;
	}
	/**
	 * Método que verifica el tamaño vertical ingresado
	 * return Tamaño vertical
	 */
	public int verificarVertical() {

		int y = 0;

		while(true){
			System.out.println("Ingrese el tamaño vertical del tablero. Nota:(y>3).");
			y = sc.nextInt();
			if(y<4){
				System.out.println("El tamaño vertical no es válido.");
				continue;
			}
			break;
		}
		return y;
	}

	/**
	 * Método que verifica el tamaño horizontal ingresado
	 * @return Tamaño horizontal
	 */
	public int verificarHorizontal() {
		int x = 0;

		while(true){
			System.out.println("Ingrese el tamaño horizontal del tablero. Nota:(x>3).");
			x = sc.nextInt();
			if(x<4){
				System.out.println("El tamaño horizontal no es válido.");
				continue;
			}
			break;
		}
		return x;
	}

	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		boolean termina = false;
		while(!termina){
			String atacante = juego.darAtacante();
			imprimirTablero();
			System.out.println("Es el turno de "+atacante);
			if(atacante.equals("CPU")){
				juego.registrarJugadaAleatoria();
			}
			else{
				registrarJugada();
			}
			termina = juego.fin();
		}
		imprimirTablero();
		System.out.println(juego.darAtacante()+" ha ganado!");
		if(reiniciar()){
			empezarJuegoMaquina();
		}
	}

	/**
	 * Método que registra la jugada del jugador
	 */
	public void registrarJugada() {

		boolean hecho = false;
		while(!hecho){
			int col = sc.nextInt();
			hecho = juego.registrarJugada(col);
			if(!hecho){
				System.out.println("Ingrese un número de columna válido");
			}
		}
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		String[][] tablero = juego.darTablero();
		for (int i = 0; i < tablero.length; i++) {
			String linea = "";
			for (int j = 0; j < tablero[i].length; j++) {
				linea+=tablero[i][j];
			}
			System.out.println(linea);
		}
	}
}
