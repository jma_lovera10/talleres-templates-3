package taller.mundo;

import java.util.ArrayList;
import java.util.Random;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;

	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;

	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;

	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;

	/**
	 * El número del jugador en turno
	 */
	private int turno;
	
	/**
	 * El número de fichas seguidas para ganar
	 */
	private int numSeguidas;

	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol, int seguidas)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="_ ";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		numSeguidas = seguidas;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}

	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}

	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		Random rand = new Random();
		int max = tablero[0].length;
		boolean hecho = false;
		while(!hecho){
			int pos = rand.nextInt(max);
			hecho = registrarJugada(pos);
		}

	}

	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		boolean hecho = false;
		if(col>=0&&col<tablero[0].length){
			for (int i = 0; i < tablero.length; i++) {
				String val = tablero[i][col];
				if(!val.equals("_ ")&&i!=0){
					tablero[i-1][col]= jugadores.get(turno).darSimbolo()+" ";
					hecho = true;
					finJuego = terminar(i-1, col);
					break;
				}
				else if(val.equals("_ ")&&i==tablero.length-1){
					tablero[i][col]= jugadores.get(turno).darSimbolo()+" ";
					hecho = true;
					finJuego = terminar(i, col);
				}
			}
		}
		if(hecho&&!finJuego){
			turno++;
			turno = turno>=jugadores.size()?0:turno;
			atacante = jugadores.get(turno).darNombre();
		}
		return hecho;
	}

	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		int contador = 1;
		boolean encontrado = false;
		boolean termina = false;
		boolean izq = true;
		boolean der = true;
		int i = 1;

		//Comprobación horizontal
		while(!termina){
			if(izq){
				if((col-i)<0){
					izq = !izq;
				}
				else{
					if(tablero[fil][col-i].trim().equals(jugadores.get(turno).darSimbolo())){
						contador++;
					}
					else{
						izq = !izq;
					}
				}
			}
			if(der){
				if((col+i)>=tablero[fil].length){
					der = !der;
				}
				else{
					if(tablero[fil][col+i].trim().equals(jugadores.get(turno).darSimbolo())){
						contador++;
					}
					else{
						der = !der;
					}
				}
			}
			i++;
			termina = (der==false&&izq==false);
		}
		encontrado = contador>=numSeguidas;
		
		if(!encontrado){
			contador = 1;
			termina = false;
			izq = true;
			der = true;
			i = 1;
			//Comprobación vertical
			while(!termina){
				if(izq){
					if((fil-i)<0){
						izq = !izq;
					}
					else{
						if(tablero[fil-i][col].trim().equals(jugadores.get(turno).darSimbolo())){
							contador++;
						}
						else{
							izq = !izq;
						}
					}
				}
				if(der){
					if((fil+i)>=tablero.length){
						der = !der;
					}
					else{
						if(tablero[fil+i][col].trim().equals(jugadores.get(turno).darSimbolo())){
							contador++;
						}
						else{
							der = !der;
						}
					}
				}
				i++;
				termina = (der==false&&izq==false);
			}
			encontrado = contador>=numSeguidas;
		}
		if(!encontrado){
			contador = 1;
			termina = false;
			izq = true;
			der = true;
			i = 1;
			//Comprobación diagonal pendiente positiva
			while(!termina){
				if(izq){
					if((fil+i)>=tablero.length||(col-i)<0){
						izq = !izq;
					}
					else{
						if(tablero[fil+i][col-i].trim().equals(jugadores.get(turno).darSimbolo())){
							contador++;
						}
						else{
							izq = !izq;
						}
					}
				}
				if(der){
					if((col+i)>=tablero[fil].length||(fil-i)<0){
						der = !der;
					}
					else{
						if(tablero[fil-i][col+i].trim().equals(jugadores.get(turno).darSimbolo())){
							contador++;
						}
						else{
							der = !der;
						}
					}
				}
				i++;
				termina = (der==false&&izq==false);
			}
			encontrado = contador>=numSeguidas;
		}
		if(!encontrado){
			contador = 1;
			termina = false;
			izq = true;
			der = true;
			i = 1;
			//Comprobación diagonal pendiente negativa
			while(!termina){
				if(izq){
					if((fil+i)>=tablero.length||(col+i)>=tablero[fil].length){
						izq = !izq;
					}
					else{
						if(tablero[fil+i][col+i].trim().equals(jugadores.get(turno).darSimbolo())){
							contador++;
						}
						else{
							izq = !izq;
						}
					}
				}
				if(der){
					if((col-i)<0||(fil-i)<0){
						der = !der;
					}
					else{
						if(tablero[fil-i][col-i].trim().equals(jugadores.get(turno).darSimbolo())){
							contador++;
						}
						else{
							der = !der;
						}
					}
				}
				i++;
				termina = (der==false&&izq==false);
			}
			encontrado = contador>=numSeguidas;
		}
		
		return encontrado;
	}

}
